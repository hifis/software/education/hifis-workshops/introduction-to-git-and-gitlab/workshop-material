<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Conflicts

In this episode, we create [a conflict](https://swcarpentry.github.io/git-novice/fig/conflict.svg) and resolve it.
In addition, we briefly introduce the typically used concepts of `branching` and `merging`.
Please see [the original Conflict episode](https://swcarpentry.github.io/git-novice/09-conflict/index.html) for further details.
In addition, [conflicts.pptx](extras/conflicts.pptx) shows an animation of all performed steps.

## Background

- `Branching` is an important concept in Git.
  - A `branch` is a specific series of commits in the repository.
  - The `master` branch reflects the current state ("truth") of the repository
    and typically acts as synchronization point for starting off new work and re-integrating finished work.
  - Every Git repository can contain as many branches as you want.
  - Every Git repository can have different branches.
  - Branches can be shared between Git repositories.
- Typically, you start a new piece of work by creating a new `branch` out of the `master` branch.
- Once, you are finished with your work you re-integrate it by `merging` your working branch back into the `master` branch.
- When re-integrating your work, it might happen that `conflicts` occur because someone else changed the same lines as you.

## Configure some Useful Details

- Configure a GitLab-like graph view in the Shell: `git config alias.graph "log --oneline --graph --all --decorate"`
- Configure a [known editor](https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Setup-and-Config#_core_editor) for later conflict resolution: `git config --global core.editor "'C:/Program Files/Notepad++/notepad++.exe' -multiInst -notabbar -nosession -noPlugin"`
  - View diff while writing commit message `git config --global commit.verbose true`
  - Test with `git config --global --edit`

## Create a new Branch and start your Work

- Show the existing branches
  - `git branch`
  - Explain the labels `master` and `HEAD`
- Show graph of the repository
  - Draw state of the repository
  - Verify with `git graph`

```
o - o <- HEAD, master
```

- Create a new branch
  - `git branch mummy-info`
- Show what changed
  - Verify with `git branch` and `git graph`
  - Explain that `*` marks where the HEAD is pointing to

```
o - o <- HEAD, master, mummy-info
```

- Switch to the newly created branch
  - `git checkout mummy-info` (Git version >= 2.23 supports: `git switch <BRANCH NAME>`)
  - Verify with `git branch`
- Edit the file `mars.txt` and commit the changes
  - Add a new sentence at the end of the file: `Mummy will appreciate sand storms on Mars`
  - Add & commit it: `git add mars.txt`, `git commit -m "Add advantages for the Mummy"`
  - Adapt the first line and attach at its end: ` (for Dracula)`
  - Add & commit it: `git add mars.txt`, `git commit -m "Concretise dracula fact"`
  - Verify with `git graph`

```
      o - o <- HEAD, mummy-info
     /
o - o       <- master
```

## Prepare the Conflict

- Switch back to the `master` branch
  - `git checkout master` (Git version >= 2.23 supports: `git switch <BRANCH NAME>`)
  - Verify with `git branch` and `git graph`
- Edit the file `mars.txt` and commit the changes
  - Add a new sentence at the end of the file: `Generating solar energy is less effective on Mars`
  - Add & commit it: `git add mars.txt`, `git commit -m "Add information about energy generation"`
  - Verify with `git graph`

```
      o - o  <- mummy-info
     /
o - o - o    <- HEAD, master
```

## Merge and Resolve the Conflict

- Merge the working branch (`mummy-info`) into the `master`branch
  - `git merge mummy-info`
  - Explain the output and the consequences
- Resolve the conflict manually with the help of the editor by editing the `<<< ... === ... >>>` section and removing the conflict markers
- Mark the conflict as resolved:
  - `git add mars.txt`
  - `git commit` (without commit message => explain interactive commit with default message) 
- Show local commit log:
  - `git graph`
  - Highlight the split with coexisting commits
  - Highlight that no version/commit is lost in a merge

```
       o -  o     <- mummy-info
      /      \
o - o - o  -  o   <- HEAD, master
```

- Clean up the local branch
  - Delete the branch `git branch -d mummy-info`
  - Verfiy with `git branch` and `git graph`

```
       o -  o
      /      \
o - o - o  -  o   <- HEAD, master
```

## Key Points

- Branching is an important concept in Git
- Git can merge diverging files line-based
  - unless the same line is changed in different ways
- Resolve conflicts by editing the `<<< ... === ... >>>` sections
  - then remove markers, `git add` and `git commit`
- Shell prompt and command outputs include hints and instructions
