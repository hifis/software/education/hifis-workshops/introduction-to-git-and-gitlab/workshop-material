<!--
SPDX-FileCopyrightText: 2021 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

<!--
This markdown file is a template for your markdown-pad. Please copy and modify the pad. Content to be changed in the source is indicated by [setup].
-->

[setup]: # (add workshop date)

# Introduction to Git and GitLab (DD./DD.MM.YYYY)

# Agenda

[setup]: # (add workshop date)

## Day 1 (DD.MM.YYYY)

- 09:00 - 09:30 Welcome & Introduction
- 09:30 - 10:30 Episodes 1 - 4: Using the Shell, Introduction to Version Control, Setting up Git, Creating a Repository
- 10:30 - 12:45 Episodes 5 - 7: Tracking Changes, Exploring History, Ignoring Things
- 12:45 - 13:00 Wrap up & Feedback

*We try to have a 15 minutes break every 90 minutes.*


[setup]: # (add workshop date)

## Day 2 (DD.MM.YYYY)

- 09:00 - 09:30 Welcome & Introduction
- 09:30 - 11:00 Episode 8: Branching and Conflicts
- 11:00 - 12:00 Episode 9: Remotes with GitLab
- 12:00 - 12:45 Episode 10: Collaboration with Others
- 12:45 - 13:00 Wrap up & Feedback

*We try to have a 15 minutes break every 90 minutes.*

# Organizational Details

## Before the Workshop

- [ ] You need a computer equipped with
	- a simple text editor (e.g. Notepad ++, gedit, ...)
	- a recent [Chromium based browser](https://en.wikipedia.org/wiki/Chromium_%28web_browser%29#Active) (e.g., Chromium, Microsoft Edge)
	- and the [Git command-line tool](https://git-scm.com/downloads).
- [ ] Please check that you have an account and can log in to the [DLR GitLab](https://gitlab.dlr.de/).
- [ ] You filled out the pre-workshop survey.


[setup]: # ("please add a link to your preferred video conferencing tool. Also feel free to add system requirements and further links \(e.g. setup guide, FAQs, troubleshooting,...\) )

- [ ] Please connect to ...
    - Please **use a recent Chromium-based browser** (e.g., Chromium, Microsoft Edge). Other modern browsers (e.g. FireFox) might work as well but we noticed sometimes performance issues.
    - Please avoid using VPN (if you use any) to improve your connection performance.
    - For further information, please see:
    
    
## During the Workshop

- Please mute yourself if you are not talking.
- Please use a headset.
- Please be back in time after breaks.
- Please ask your questions in the chat during the presentation.


# Open Questions

## How to edit the commit message later?

- For the last commit, you can to this by using `git commit --amend`. Be careful as it changes the COMMIT ID.
- For other commits you have to do an interactive rebase. But be careful as you are changing the version history (COMMIT IDs). For more information, see here: https://www.sitepoint.com/git-interactive-rebase-guide/


## What belongs into the Git repository?

- https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/blob/master/episodes/01_put-your-code-under-version-control.md#what-belongs-into-the-repository provides some hints

[setup]: # (add workshop date)

# Day 1 - DD-MM-YYYY


## Introduction Round

- Please tell us *in a minute* about your background and interest in the workshop.
- You can switch on your camera, if you like **:)**




## [Using Shell](https://gitlab.com/hifis/hifis-workshops/introduction-to-git-and-gitlab/workshop-material/-/blob/master/episodes/01-using-shell.md)

- Open a shell (bash, Git Bash) which allows you to run the Git command line tool 

- Try out some commonly used commands
  - `pwd`: print working directory - shows the current path
  - `mkdir <folder name>`: make directory - creates folders with the name "folder name"
  - `ls`: list - display the contents of the current directory
  - `ls -la`: list, `-l` = list format, `-a` = all - show the contents of the current directory as a list with additional information
  - `<command> --help`: Displays help pages for most commands
  - `cd <folder name>`: change directory - change to the specified directory
  - `touch <file name>`: Create an empty file in the current directory
  - `cat <file name>`: Output of the file content
  - `less <file name>`: Output of the file with pagination, exit with *Q*
  - `rm <file name>`: removes specified file
- Additional commands:
  - `cd ..`: change directory - change to parent directory
  - `nano <file name>`: Editor for editing files
  - `rm -r <file name>`: remove, `-r` = recursive - delete specified folder with all subfolders and files
- Additional tips:
  - No news are good news
  - With the Tab key, file names can be auto-completed 
  - `cat <filename> | less`: With the pipe symbol (`|`) the output of one command can be introduced into another



At the end you should have prepared the following:
- Opened a shell in which you can run the Git command
- Created a directory for the workshop (e.g., `mkdir workshop`)
- Switched into your workshop directory (e.g., `cd workshop`)


## [Introduction to Version Control](https://swcarpentry.github.io/git-novice/01-basics/index.html)

!["Final".doc](https://swcarpentry.github.io/git-novice/fig/phd101212s.png)


### Main Points
- Git keeps track of file changes.
  You are able to decide which of these changes make the next file version.
  A record of these changes is called a **commit**.
  These commits automatically include useful metadata (e.g., author, date and time of the change etc.).
- Git allows you to organize related files in a **repository** (e.g., code and documentation of a software, a paper, ...) including their history of changes and related metadata.
- Repositories can be kept in sync across different computers and support collaboration among different people.

## [Setting up Git](https://swcarpentry.github.io/git-novice/02-setup/index.html)


### Main Settings

> This information ends up in the version control system later! Please make sure that you configured it consistently accross your (client) systems.

- `git config --global user.name "Last name, first name"`: Sets the username for the whole system (all Git repositories)
- `git config --global user.email "firstname.surname@organization.de"`: Sets the email address for the whole system (all Git repositories)
- `git config --global --list`: Displays the global configuration settings
- `git config --list`: Displays the complete configuration settings (including system settings)

### Other useful Git Settings

- [Configure alternative editors](https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Setup-and-Config#_core_editor)
- `core.autocrlf = true` this option is usually set by the installation to suit the respective system. 


## [Creating a Repository](https://swcarpentry.github.io/git-novice/03-create/index.html)

> We create a repository to collect information about different planets important for our clients Dracula, Wolfman and the Mummy.

![](https://swcarpentry.github.io/git-novice/fig/motivatingexample.png)


- `mkdir planets` - Create the directory `planets`
- `cd planets` - Change into the directory `planets`
- `git init` - Initialize the Git repository
- `ls -la` - Shows the (hidden) `.git` directory
- `.git` - This directory contains all data and metadata of the current git repository. If you delete this directory, the history of all your changes is lost. Please be careful with it! In addition, you can for instance, keep their configuration settings sepecific to this repository.

### Git`s default main branch

- A branch is a series of commits.
- The name of default branch in Git is still named `master`.
- However, the software development community has moved to adopt more inclusive language instead.
- For that reason most Git code hosting services transitioned to using `main` as the default branch.
- If you want to have this behaviour with `git init` as well, you can configure it via `git config --global init.defaultBranch main` (since Git 2.28)
- Otherwise you can rename the branch via: `git branch -m master main`

### Hints

- You can now do local configuration for this repository, like author infos only for a certain repository:
  - `git config user.name "Dracula, Vlad"`
  - `git config user.email "vlad.dracula@transylva.ninja"`
  - (Note the absence of `--global`)
- `git config --list` will show you *all* settings that apply for the current repository, even those that were overwritten. The last one in the list is the one that will be active.


## [Tracking Changes](https://swcarpentry.github.io/git-novice/04-changes/index.html)




### Commit Cycle 1: Add a new file to the repository


> First, we want to collect information about planet Mars.
> - `touch mars.txt` - Create the file `mars.txt`

- `git status` helps us to ensure we're in the repository folder ("`On branch main`") and everything is *clean* ("`noting to commit`")

- Edit the file `mars.txt` and add the following:
  - Add one new line: `Cold and dry, but everything in my favorite color` 
 
  - Add a closing blank line at the end of the file. It is important to make sure that the different Git commands work properly.

  - Please make sure that the file is properly saved.

- `git status` - View the current status of the `planets` repository: 
  - There is one new file.
  - This file is not yet under version control (red color).

- `git add mars.txt` - Add the new file to the repository 

- `git status` - View the changes in the `planets` repository again: 
 
  - There is one new file.
  - This file has been put under version control (green color).
  - This file is not yet committed.

- `git commit -m "Start notes on Mars as base"` - Create a commit which records the new file in the history of the repository.

- `git log` - Display the history of the `planets` repository which shows one commit including its commit ID (full SHA-1 hash), the commit author (configured global name and email address), the commit date, and the commit message (full message). 

- `git status` - View the changes in the `planets` repository again. There are no changes anymore. 



### Commit Cycle 2: Change an existing file in the repository

> Now, we want to collect further information about Mars with regard to Wolfman.

- Edit the file `mars.txt` and add the following:
  - Add one new line: `The two moons may be a problem for Wolfman`
  - Add a closing blank line at the end of the file.
  - Please make sure that the file is properly saved.
 
- `git status` - The command output shows that the file` mars.txt` has been modified (red color).
- `git diff` - The command output shows us the changes between the different versions. In the output, below `index`, it is shown which exact versions are compared.

  - `git add mars.txt` - Add the changes to the repository and mark them for the next commit. 
  - `git commit -m "Add concerns about effect of Mars' moons on Wolfman"` - Create the second commit which records the added new line in the history of the repository.

- We performed the second time the basic Git change workflow (Modification => Staging Area => Repository History):
 ![Basic Git Change Workflow](https://swcarpentry.github.io/git-novice/fig/git-staging-area.svg)


### Commit Cycle 3: Change an existing file and experiment with `git diff`


> Now, we want to collect further information about Mars with regard to the Mummy.


- Edit the file `mars.txt` and add the following:
  - Add one new line: `But the Mummy will appreciate a lack of humidity`
  - Add a closing blank line at the end of the file.
  - Please make sure that the file is properly saved.
- `git add mars.txt` - Add the changes to the repository and mark them for the next commit.

- `git diff` - The command output does not show any differences because there is no difference between the file in the working directory and the staging area.

- `git diff --staged` - The option `--staged` shows the differences between the staging area and the last commit. In our case it shows the newly added line.

- `git commit -m "Discuss concerns about Mars' climate for Mummy"` - Create the third commit which records the added new line in the history of the repository.

- `git log` - Display again the current repository history. It shows all commits (detailled view).

- `git log --oneline` - The option `--oneline` shows a reduced version of the output. This is one reason why a good first commit message line is so important.

<!--
- Edit the mars.txt somewhere
- `git status` shows the mars.txt as staged and and not staged
- `git diff` shows the recently but not staged changes
- `git diff --staged` only shows the changes that were added to the staging area
<!--
- Remove the change in the mars.txt
- `git diff` does not return anything
- `git diff --staged` still shows the change in the staging area
- `git status` shows the mars.txt as staged




- We performed the third time the basic Git change workflow (Modification => Staging Area => Repository History):
 ![Basic Git Change Workflow](https://swcarpentry.github.io/git-novice/fig/git-committing.svg)

-->
### Quiz

- [Commit Messages](https://swcarpentry.github.io/git-novice/04-changes/index.html#choosing-a-commit-message)
- [Commit Changes to Git](https://swcarpentry.github.io/git-novice/04-changes/index.html#committing-changes-to-git)


### Additional tips

- You can use `git add -i` or `git add --interactive` to fine-tune what goes into the staging area. With that you can also add single lines of file as well. [This tutorial](https://coderwall.com/p/u4vjkw/git-add-interactive-or-how-to-make-amazing-commits) provides a brief overview of its usage.
- More information about "good" commit messages: https://chris.beams.io/posts/git-commit/


## [Exploring History](https://swcarpentry.github.io/git-novice/05-history/index.html)

> Now we want to navigate the commit history.

- Edit the file `mars.txt` and add the following:
  - Add one new line: `An ill-considered change`
  - Add a closing blank line at the end of the file.
  - Please make sure that the file is properly saved.

- `git diff HEAD mars.txt` - Compares the changed file with the last committed version (third commit)

- `git diff HEAD~1 mars.txt` - Compares the changed file with the version the second last committed version (second commit) 
- `git diff HEAD~2 mars.txt` - Compares the changed file with the version the third last committed version (first commit) 
- `git diff HEAD~3 mars.txt` - Errors, since no commit/version exists here (Before first commit)

> `HEAD~<NUMBER>` allows us to navigate the commit history relatively starting from the last commit (`HEAD`). But you can also use the commit ID for this purpose.


- `git show HEAD~2 mars.txt` - Shows the commit ID as well. You can use this commit ID instead of the relative addressing scheme:
  - Copy the commit ID. Normally, you only require the first 7 digits of it to identify a commit.
  - `git diff <copied commit ID> mars.txt` - Shows the same output as `git diff HEAD~2`
- `git status` - Shows that there is still the accidently changed `mars.txt` which we want to undo.

- `git checkout HEAD mars.txt` - Replaces the currently changed `mars.txt` with the last committed version and undos the accidental change (This is not recommended anymore, but still working).
- Newer versions of the git software use `git restore mars.txt`

> **Warning**: `git checkout` or `git restore` might permantely remove your local changes without the chance to undo this.

- You can even go back further into the past of your commit history:
  - `git checkout HEAD~2 mars.txt` - Restores the initially committed version of `mars.txt` (just one line)

  - `git status` - Indicates an already staged change of `mars.txt`
  - `git diff` - Shows nothing because there are no local changes
  - `git diff --staged` - Shows the staged changes (removes two lines)
  - `git checkout HEAD mars.txt` - Restores the last committed version of `mars.txt` (all three lines). Puh, everything is fine again :)




| Command           | Working Directory | Staging Area | Repository |
| ----------------- | ----------------- | ------------ | ---------- |
| git diff          | A                 |              | B          |
| git diff --staged |                   | A            | B          |
| git diff HEAD     | A                 |              | B          |

### Don't lose your head!

- If you accidently type `git checkout <unique ID of commit>` (without file/directory name), you will end in the 'detached head' state.
- In this status you should make no changes as they might be lost.
- You can re-attach your head via `git checkout main` or `git switch main` (Git version >= 2.23). 

### Quiz

- [Recovering Older Versions of a File](https://swcarpentry.github.io/git-novice/05-history/index.html#recovering-older-versions-of-a-file)
- [Understanding Workflow and History](https://swcarpentry.github.io/git-novice/05-history/index.html#understanding-workflow-and-history)


## [Ignoring Things](https://swcarpentry.github.io/git-novice/06-ignore/index.html)

> Now, we want to make sure that some temporarily created files are not accidently committed to the repository.

- `mkdir results` - Creates the directory `results`
- `touch a.dat b.dat c.dat results/a.out results/b.out` - Creates different temporary files
- `git status` - Shows three new files and one new directory

- `touch .gitignore` - Creates the file `.gitignore` which we can use to specify file name patterns of files we want to ignore.
- Edit the file `.gitignore`, add the following content and store it:
  ```
  *.dat
  results/

  ```

- `git status` - Shows only the new file `.gitignore`. The other files are ignored.

- `git add .gitignore` - Add the file to the staging area.

- `git commit -m "Ignore data files and the results folder"` - Creates a new commit.

- `git status` - Indicates that here are no changes

- `git add a.dat` - `a.dat` - **will not be added** and is still ignored.

- `git status --ignored`  shows all files that are currently ignored.

### Quiz

- [Ignoring Nested Files](https://swcarpentry.github.io/git-novice/06-ignore/index.html#ignoring-nested-files)
- [The Order of Rules](https://swcarpentry.github.io/git-novice/06-ignore/index.html#the-order-of-rules)

<!--
- `git add -f a.dat` - `a.dat` - forces to add the file ignoring the rules in .gitignore

-->

### Links

- https://gitignore.io allows you to generate your specific `.gitignore` file.


[setup]: # (add workshop date)

# Day 2 - DD-MM-YYYY

## Welcome & Introduction

## [Conflicts](https://gitlab.com/hifis/hifis-workshops/introduction-to-git-and-gitlab/workshop-material/-/blob/master/episodes/episodes/08-conflicts.md)


### Basics

#### Branching

- Branching is an important concept in Git.
- A branch is a specific series of commits in the repository.
- Every Git repository can contain as many branches as you want.
- Every Git repository can have different branches.
- Branches can be shared between Git repositories.
- Visualization example: http://git-school.github.io/visualizing-git/

![](https://pad.gwdg.de/uploads/upload_a3ebb82690036691473f4e2614623d12.png)

#### Workflow and Conflicts

- The main branch reflects the current state ("truth") of the repository and typically acts as synchronization point for starting off new work and re-integrating finished work.
- Typically, you start a new piece of work by creating a new branch out of the **main branch**.
- Once, you are finished with your work you re-integrate it by merging your working branch back into the **main branch**.
- When re-integrating your work, it might happen that conflicts occur because someone else changed the same lines as you.


### Check you are at the right place
- `pwd` - Check you are in the planets folder, if not switch to it using `cd planets`
- `git status` - Check your git is clean
- `git log --oneline` - Check if the commits are there

### Configure some Useful Details
- `git config --global alias.graph "log --oneline --graph --all --decorate"` - Configures a GitLab-like graph view in the shell

- `git config --global commit.verbose true` - Configures Git to give verbose information about what will be commited in th editor

<!--
- `git config --global --edit`- Test your editor configured for Git, other editor configuration: https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Setup-and-Config#_core_editor
-->
### Create a new Branch and start your Work

- `git graph` - Shows the current graph: 4 commits on branch `main`, `HEAD` points to the last commit

> Provide more information about the Mummy while working on a separate branch ("feature branch").
- `git branch` - Shows the defined local branches: only `main`
- `git branch mummy-info` - Creates the branch for our work to do but we are still on `main`
- `git branch` - Shows two branches now
- `git checkout mummy-info` (Git version >= 2.23 supports: `git switch <BRANCH NAME>`) - Switch to the new branch
- `git branch` - Indicates that we are now on branch `mummy-info`
- `git graph` - Shows a new label for the created branch, `HEAD` still points on `main`

- Edit the file `mars.txt` and commit the changes
  - Add a new sentence at the end of the file (do not forget the new line!): `Mummy will appreciate sand storms on Mars`
  - Add & commit it: `git add mars.txt`, `git commit -m "Add advantages for the Mummy"`
  - Adapt the first line and attach at its end: ` (for Dracula)`
  - Add & commit it: `git add mars.txt`, `git commit -m "Concretize the first fact"`
  - Verify with `git graph`

### Prepare the Conflict

> In the meanwhile, someone else changed `mars.txt` on `main`

- Switch back to the `main` branch
  - `git checkout main` (Git version >= 2.23 supports: `git switch <BRANCH NAME>`)
  - `git branch` - Indicates that we are back on `main`
  - `git graph` - Shows the `mummy-info` branch evolved and has two more commits
- Edit the file `mars.txt` and commit the changes
  - Add a new sentence at the end of the file (do not forget the new line!): `Generating solar energy is less effective on Mars`
  - Add & commit it: `git add mars.txt`, `git commit -m "Add information about energy generation"`
  - `git graph` - Indicates a split in the commit history because there are **two different commits** which are based on the same parent commit.

### Merge and Resolve the Conflict

> Now, we want to integrate our work back into the main branch.

- There are several ways to do this. Here we use the classic standard way via `git merge` which preserves the branching structure. In contrast to `git rebase`, which we won't cover here.
- For a merge, you start on the branch into which the changes should be integrated (target branch, here `main`). Let us start...

- `git merge mummy-info` starts the merge process and shows the following output:
  - `Auto-merging mars.txt` - Git has merged automatically. This should be the 1st line.
  - `CONFLICT (content): Merge conflict in mars.txt` - Something could not be automatically merged by Git. This should be line 4.
  - `Automatic merge failed; fix conflicts and then commit the result.` - Prompts us what we have to do now.

- `git status` - Tells us more about what we can do next:
  - `  (fix conflicts and run "git commit")` - We can resolve the conflicts and commit the result.
  - `  (use "git merge --abort" to abort the merge)` - We can abort the merge and go back to the start.
  - The following output shows us the conflicting file paths and tells us how to indicate the conflict resolution:
    
    ~~~
    Unmerged paths:
       (use "git add <file>..." to mark resolution)
       both modified:   mars.txt
    ~~~
- Open `mars.txt` in your editor and resolve the conflict manually:
  - `<<<<<<< HEAD` shows the start of the conflict. Below you find you find the version on which our `HEAD` points to.
  - `=======` separates the `HEAD` version from the version on branch `mummy-info`
  - `>>>>>>> mummy-info` indicates the end of this conflict marker. However, in practice there could be more.
  - We decide to keep both lines, remove the conflict markers and save our changes.
- `git add mars.txt` - Tells Git that the conflict has been resolved.
- `git commit` - Opens the configured Git editor and shows a default merge commit message. We use this message and close the editor. Now both branches are merged.

- `git graph` - Shows that both branches have been merged via a merge commit:
  - You can still identify the branch `mummy-info``including its two commits.
  - You can see that the split has been merge via the merge commit.

- Finally, we want to clean up the feature branch
  - `git branch -d mummy-info` - Deletes the branch `mummy-info`.
  - `git graph` - Shows the same output. Only the label `mummy-info` is no longer there.

### Restore the Feature Branch (Demonstration only)

- `git checkout <ID of the last commit of the removed branch` - Brings us into the detached HEAD mode with the final content of the branch `mummy-info`
- `git branch mummy-info` - Restores the branch `mummy-info`
- `git checkout mummy-info` - Let us switch restored branch

- Lessons learnt
  - **As long as the commit object exists, you can start from it again.**
  - Git branches are labels for a specific commit line in your repository.


## [Remotes with GitLab](https://gitlab.com/hifis/hifis-workshops/introduction-to-git-and-gitlab/workshop-material/-/blob/master/episodes/episodes/09-remotes-in-gitlab.md)


> Now, we want to (potentially) share our local Git repositories with other via GitLab.

- So far we have only worked locally. Usually, however, you work with several Git repositories, between which you synchronize.
- We are about to introduce another repository, which is managed by GitLab. This could also be managed by GitHub or another local Git repository.
- Repositories that are not on your own hard drive are called "remote repositories".
- See also for different repository topologies: https://git-scm.com/about/distributed 


### Publish the local Git repository in GitLab

#### Create a new Gitlab Project

- Open https://gitlab.com/users/sign_in and log in with your credentials (use a recent Web browser version)

- Click the `+ => New project` (toolbar on the top) which opens the dialog for creating a new project:
  - Select `Create blank project`
  - `Project name` => `planets`
  - `Project description` => `Collects information about new home planets.`
  - Click `Create project` which creates a new GitLab project and redirects you to its overview page.
- Further remarks:
  - The `Project URL` allows you to select different namespaces (groups, subgroups, your username namespace). We create a "personal" project in your user namespace. Later projects can also be moved to other namespaces.
  
  - Even if the `Project description` is optional, please use them because they make it easier to navigate in project lists later.
  - Visibility allows to define different levels:
    - `private`: Only explicitly added project members can see and do something with the GitLab project.
    - `internal`: Only authenticated users of the GitLab instance can see/clone/fork the GitLab project. (not available on GitLab.com)
    - `public`: The GitLab project is visible/cloneable even for unauthenticated users.

  - We **do not** initialize the GitLab project with a `README.md` file. Otherwise we have problems pushing our local Git repository directly to our GitLab project. 
  - When creating the GitLab project, GitLab runs a `git init --bare` in the background.
  - GitLab project = Git repository + a lot of further useful tools

- On the GitLab project page:
  - Here files can be created directly (README, LICENSE. CHANGELOG, ...). We don't use this feature. Otherwise we have problems pushing our local Git repository directly to our GitLab project. But later we might use them :)
  - There are also references to steps that can be carried out (create new repository, link existing folder, push existing repository). automatically GitLab infers the right protocol (HTTPS, SSH) from your user account preferences. We will use the HTTPS protocol for transferring our repository.



#### Push the local Git Repository to your GitLab Project
- Click `Clone => Clone with HTTPS` and use the copy button on your GitLab project page.
- Switch again to your Git shell.
- `git status` - Please make sure that there are no uncommitted changes and that your on branch `main`.
- `git remote add origin <copied URL>` - Creates a link to the remote Git repository on GitLab.com. URL should look similar to: `https://gitlab.com/<username>/planets.git`
- `git remote` - Only shows the defined remote labels.
- `git remote -v` - Displays two entries for `origin`, one where we 'push' changes to and another from where we 'fetch' changes. These URLs are usually the same. Different URLs and thus repositories are possible, but are only used in special sync workflows. We do not explain them here.
- So far we have only created the link, but not yet used it. For that we need network access and have to authenticate ourselves in GitLab. 

- `git push` - pushes the content of our repository but shows an error message because we have not connected the branches. Luckily, Git tells us what to do.
- `git push --set-upstream origin main` - Instructs Git to link the current branch the branch `main` in repository called ` origin` (which is on Gitlab). Link is created and data is transferred. Output shows that a new branch was created on the remote repository and that the files were transferred. 
- Reload the GitLab project page. Now you can see the files and their history on GitLab. Congrats :)
- Click the `History` button in the GitLab file view. It shows the same entries that are shown by `git log --oneline`
- `git branch` - Displays only local branches.
- `git branch --all` - Displays remote branches as well.
- Via Repository > Files > Edit Button you can change files with the web IDE, stage and commit changes.


### Synchronize the local Git Repository with changed Content

#### Add a new File via the GitLab Web Interface

- In the GitLab file view of your project, we create a new file via `+ => New file`.
- Create a new file named `venus.txt`
  - Add the line `Wolfman will appreciate the absence of moons`  and an empty new line at the end of the file.
   - Scroll down and set the `Commit message`: `Start notes on Venus`
   - You could change the branch, but we stay on main.
   - Click `Commit changes` which runs a `git add` and a `git commit` in the background.
- Switch back to the overview page (e.g., via the upper navigation list, click on your project name). Now the new file is displayed.
- We are now editing the `mars.txt`. Click on the file in the browser. Use the blue "Edit" button at the top right. Changes can be made in the browser right away. Alternatively, you can use the Gitlab Web IDE (button next to it).
  - Append the line `We have to bring our own oxygen`.
  - Commit the change using the commit message `Add notes about the Mars atmosphere`. 


#### Synchronize your local Git Repository

- `git graph` - Only shows our local commits but not the changes we did in GitLab. These commits are so far only in the repository on Gitlab.
- `git fetch origin main` - Checks if there is anything new in the remote repository on branch `main`. The output shows that something was fetched.
- `git graph` - Now we see the 1 new commit objects in the graph and our `HEAD` still points to the latest local commit.
- `git status` - Tells us that we are with our local `main` branch 1 commits behind the `main` on the remote repository (`origin/main`). In addition, the output tells us that we can use `git pull` to retrieve them.


- `git pull` - Now we retrieve the commits and apply them to our local `main` branch. You normally run `git pull` directly. But in this case everything is directly applied to your local branch.`git fetch` allows you to view the changes first and then to decide whether the changes are wanted. For example, you could display them via `git diff main origin/main`.
- If you use `git pull`, it implicitly performs a `git merge` as well. The `git status` hint that it `can be fast-forwarded` tells us it will not create a merge commit and more or less treat the two branches as one.
- `git status` - Indicates that everything is clean again
- `git graph` - Now both `main` branches point to the latest commit.
 

- We now have tried both directions (push/pull) to synchronize our repository: https://rlogiacco.wordpress.com/2017/04/05/git-tricks/


## [Collaboration with Others](https://gitlab.com/hifis/hifis-workshops/introduction-to-git-and-gitlab/workshop-material/-/blob/master/episodes/10-collaboration-with-others.md) (Demonstration only)

- A remote Git repository and, particularly, a GitLab project allows us to collaborate with other people.
- Project members can be added via the `Members` menu (left toolbar). An invitation email is sent to the invitee by GitLab.
- For larger projects it is recommended to create your own GitLab group.
- In the following, we will use our previous example to show how you can work together using a GitLab project using merge requests.
- Example scenario: "Someone wants to contribute a README file with basic documentation."


### How can the Contributor get Access?

- Add the contributor via the `Members` menu to the project.
- A contributor could contribute via the [Fork workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) if your project is visible (public, internal).
- We demonstrate the first option.


### Contributor View: Provide some Documentation

#### Clone the remote Repository

- Open the GitLab project page and copy the Git URL of the remote Git repository.
- Open your Git shell and change into a directory which is not a Git repository yet.
- `git clone <copied URL> <Local Repository Name>` - Clones the remote Git repository and creates a local Git repository. The local repository name is optionally but is useful to avoid "name clashes". In the local repository, the remote `origin` (= place where it comes from) and the branches are already correctly linked.
- `ls` - Shows the new Git repository.
- `cd <Local Repository Name>` - Switches into the new repository.
- `git graph` - Shows all commits and branches that have been created so far.


#### Create the Contribution

- `git branch documentation` - Create `documentation` branch.
- `git switch documentation`Switch to the `documentation` branch
- `git graph` - Tells us that we are working on the right branch.
- `touch README.md` - Creates an empty `README.md` file.
- Open the file in an editor and add the following content:
  ```
  # Planets
  
  This repository contains notes about potential planets for migration.
  
  ```
- `git add README.md` + `git commit -m "Add an initial description for the project"` - Commits the documentation on the local branch.
- `git graph` - We are one commit ahead of our local `main` branch.
- `git push` - Shows again an error because the local branch is not mapped to the remote repository. Fix it via:`git push --set-upstream origin documentation`


#### Create a Merge Request

- Usually, you could again merge locally and publish it using
  - `git switch main` to change to main again
  - `git merge documentation` to locally merge the changes
  - But `git push` will fail, because I'm not allowed to write to main, as the branch is *protected*

- Switch to the GitLab project page
- Open `Repository => Branches`
- Select `Merge request` for the `documentation` branch.
- Alternative: Open the URL shown in the output of `git push` directly in the Web browser.
- On the `New Merge Request` page you can do the following:
   - Change branches: Select different source and target branches 
   - Title: Use a speaking name, Start with `Draft:` to indicate work in progress
   - Description:
       - Describe what was done, what purpose the branch serves
       - Specify here (via a `Task List`) which work is still missing (especially helpful with drafts)
     - People and milestones, etc. can be added to the merge quest
     - Rules can be specified that must be met before a merge is possible
     - Create via `Submit merge request`
- With the status `Draft` a merge is not yet possible. I.e., the status must first be changed (edit title or use `Mark as ready` button) to "ready". 

### Owner View: Review the Merge Request 

- On the Gitlab page of a merge request, you can
  - discuss about the changes,
  - review the changes,
  - approve the changes and much more.
- After the successful merge, the branch is no longer available in the remote Git repository (default behaviour).

### Contributor View: Update and Clean up the local Git Repository

- Switch to your Git shell.
- `git checkout main` (or `git switch`) - Switch into the `main` branch.
- `git pull` - Changes from remote repository are applied locally.
- `git branch -d documentation` - Deletes the branch `documentation`.
- With `git remote prune origin` or` git fetch --prune` the references to outdated remote branches in `origin` can be removed as well.


### Bonus I: Initiate Merge Request via an Issue

- Switch to the GitLab project page
- Create a new issue via `+ => New issue`
- At least, add a title such as `Extend documentation` and create the issue via `Submit issue`.
- You can let GitLab create a branch and merge request via the `Create merge request` button.


### Bonus II: Mark the current Results with a Tag

- Tags are pointers to record certain versions using a speaking name/label (because the commit IDs are not so speaking ;) ).
- In this way, for example, certain versions / releases can be specified / marked.
- Creation via the git command:
  - `git tag v0.1 -m "Mark initial version"`
  - `git push origin v0.1`
- Creation via GitLab:
  - Select `Repository => Tags` (left toolbar)
  - Select `New tag`
  - At least specify the `Tag name` and click `Create tag` 
- The tag can be used to specifically access the commit version without using a commit ID or the `HEAD` addressing scheme.
- Tags are usually set by a project owners / maintainers.
- You can learn more about release numbers here: https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/blob/master/episodes/05_mark-the-stable-version-of-your-code.md
- For more informaton on tagging, please see: https://git-scm.com/book/en/v2/Git-Basics-Tagging

# Further Readings

- [Workshop Materials on GitLab.com](https://gitlab.com/hifis/hifis-workshops/introduction-to-git-and-gitlab/workshop-material)
- [Git Cheat Sheet from GitLab Inc.](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
- [Official Git Reference](https://git-scm.com/doc)
- [Flight Rules for Git](https://github.com/k88hudson/git-flight-rules)
- [First Aid Git](http://firstaidgit.io)
- [Interactive Git Cheat-Sheet](http://ndpsoftware.com/git-cheatsheet.html)
- [gitignore.io - Create .gitignore patterns](https://gitignore.io)
- [Martin Fowler: Patterns for Managing Source Code Branches](https://martinfowler.com/articles/branching-patterns.html)
- [Visualizing Git](https://git-school.github.io/visualizing-git)

## Wrap up

[setup]: # (survey link)

- Please fill out our post [workshop survey]() :)
- Similar courses are offered by HIFIS (Helmholtz wide). Keep an eye on [https://hifis.net/events]()
