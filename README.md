<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Version Control using Git 

This workshop provides an introduction into Git in combination with the collaboration platform GitLab.
The workshop curriculum is heavily based on:

> Madicken Munk, Katherine Koziar, Katrin Leinweber, Raniere Silva, François Michonneau,
> Rich McCue, ... Wolmar Nyberg Åkerström. (2019, July).
> swcarpentry/git-novice: Software Carpentry: Version Control with Git.
> Zenodo. https://doi.org/10.5281/zenodo.3264950

Additionally, we included some extra episodes, particularly, to show the interplay of Git with GitLab.

## Prerequisites

No special prior knowledge is required to participate in this workshop.

## Setup

Please bring your laptop including a Web browser, a text editor, and a recent [Git command line client](https://git-scm.com/downloads).

If you want to set up a workshop based on this materials, please see the [Workshop Setup Information](setup/).

## Curriculum

1. [Using the Shell](episodes/01-using-shell.md)
1. [Introduction to Version Control](https://swcarpentry.github.io/git-novice/01-basics/index.html)
<br />*Figures:* ["Final".doc](https://swcarpentry.github.io/git-novice/fig/phd101212s.png)
<br />*Extras:* [GitLab Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
1. [Setting up Git](https://swcarpentry.github.io/git-novice/02-setup/index.html)
<br />*Extras:* [Configure alternative editors](https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Setup-and-Config#_core_editor)
1. [Creating a Repository](https://swcarpentry.github.io/git-novice/03-create/index.html)
<br />*Figures:* [Universal Missions](https://swcarpentry.github.io/git-novice/fig/motivatingexample.png)
1. [Tracking Changes](https://swcarpentry.github.io/git-novice/04-changes/index.html)
<br />*Figures:* [Modify Add Commit Workflow](https://swcarpentry.github.io/git-novice/fig/git-staging-area.svg),
[RECAP: Modify Add Commit Workflow](https://swcarpentry.github.io/git-novice/fig/git-committing.svg)
<br />*Exercises:* [Choosing a Commit Message](https://swcarpentry.github.io/git-novice/04-changes/index.html#choosing-a-commit-message),
[Committing Changes to Git](https://swcarpentry.github.io/git-novice/04-changes/index.html#committing-changes-to-git)
1. [Exploring History](https://swcarpentry.github.io/git-novice/05-history/index.html)
<br />*Figures:* [Git Checkout Scheme](https://swcarpentry.github.io/git-novice/fig/git-checkout.svg),
[Git Staging Scheme](https://swcarpentry.github.io/git-novice/fig/git_staging.svg)
<br />*Exercises:* [Recovering older Version of a File](https://swcarpentry.github.io/git-novice/05-history/index.html#recovering-older-versions-of-a-file),
[Understanding Workflow and History](https://swcarpentry.github.io/git-novice/05-history/index.html#understanding-workflow-and-history)
1. [Ignoring Things](https://swcarpentry.github.io/git-novice/06-ignore/index.html)
<br />*Exercises:* [Ignoring Nested Files](https://swcarpentry.github.io/git-novice/06-ignore/index.html#ignoring-nested-files),
[The Order of Rules](https://swcarpentry.github.io/git-novice/06-ignore/index.html#the-order-of-rules)
1. [Conflicts](episodes/08-conflicts.md)
1. [Remotes in GitLab](episodes/09-remotes-in-gitlab.md)
1. [Collaboration with Others](episodes/10-collaboration-with-others.md)

### Schedule

| Time | Topic |
| ------ | ------ |
| Before the workshop | [Setup](#Setup) |
| 09:00 - 09:30 | Welcome & Introduction |
| 09:30 - 12:30 | Git / GitLab Part 1 (episodes 1 to 7) |
| 12:30 - 13:30 | *Lunch Break* |
| 13:30 - 16:45 | Git / GitLab Part 2 (episodes 8 to 10) |
| 16:45 - 17:00 | Wrap Up & Feedback |

*The actual schedule may vary slightly depending on the interests of the participants.*

### Further Readings

- [Official Git Reference](https://git-scm.com/doc)
- [Flight Rules for Git](https://github.com/k88hudson/git-flight-rules)
- [First Aid Git](http://firstaidgit.io)
- [Interactive Git Cheat-Sheet](http://ndpsoftware.com/git-cheatsheet.html)
- [Martin Fowler: Patterns for Managing Source Code Branches](https://martinfowler.com/articles/branching-patterns.html)

## Contributors

Here you find the main contributors to the material:

- Tobias Schlauch
- Carina Haupt
- Michael Meinel
- Martin Stoffers
- Katrin Leinweber
- Benjamin Wolff

## Contributing

Please see [the contribution guidelines](CONTRIBUTING.md) for further information about how to contribute.

## Changes

Please see the [Changelog](CHANGELOG.md) for notable changes of the material.

## License

Please see the file [LICENSE.md](LICENSE.md) for further information about how the content is licensed.
